/**
 * 图片上传数量及其大小等控制
 * 点击开始上传按钮(test9)，执行上传
 * 
 */
var success=0;
var fail=0;
var imgurls="";

$(function (){
	var imgsName="";
	layui.use(['upload','layer'],function() {
		var upload = layui.upload;
		var layer=layui.layer;

		upload.render({
			elem: '#test1',
			url: '/getData/upload',
			multiple: true,
			auto:false,
//			上传的单个图片大小
			size:10240,
//			最多上传的数量
			number:5,
//			MultipartFile file 对应，layui默认就是file,要改动则相应改动
			field:'file',
			bindAction: '#test9',
			before: function(obj) {
				//预读本地文件示例，不支持ie8
				obj.preview(function(index, file, result) {
					$('#demo2').append('<img src="' + result 
						+ '" alt="' + file.name 
						+'"height="92px" width="92px" class="layui-upload-img uploadImgPreView">')
				});
			},
			done: function(res, index, upload) {
				//每个图片上传结束的回调，成功的话，就把新图片的名字保存起来，作为数据提交
				console.log(res.code);
				if(res.code=="1"){
					fail++;
				}else{
					success++;
					if(imgurls ==""){
                        imgurls += res.data.src;
					}else {
                        imgurls += ","+res.data.src;
                    }
					$('#imgUrls').val(imgurls);
				}
			},
			allDone:function(obj){
			    layer.msg("总共要上传图片总数为："+(fail+success)+"\n"
			    			+"其中上传成功图片数为："+success+"\n"
			    			+"其中上传失败图片数为："+fail
			    		 )
			}
		});

	});
	
	//清空预览图片
	cleanImgsPreview();
	//保存商品
	goodsSave();
});

/**
 * 清空预览的图片
 * 原因：如果已经存在预览的图片的话，再次点击上选择图片时，预览图片会不断累加
 * 表面上做上传成功的个数清0
 */
function cleanImgsPreview(){
	$("#cleanImgs").click(function(){
		success=0;
		fail=0;
		$('#demo2').html("");
		$('#imgUrls').val("");
	});
}
var sname;
var price;
/**
 * 保存商品
 */
function goodsSave(){
	$('#btnSubmit').click(function(){
		sname=$("#title").val();//名称
		var stype=$("#stype").val();//类别id
		var imges=$("#imgUrls").val();//上传的相片名称
		var descript=$("#descript").val();//描述
		price=$("#price").val();//起拍价格
		var cou=$("#cou").val();
		var start=$("#start").val();
		if(!sname){
			layer.alert("请输入拍品名称");
			return;
		}
		if(!descript){
			layer.alert("请输入拍品描述");
			return;
		}
		if(!price){
			layer.alert("请输入起拍价格");
			return;
		}
		if(!sname){
			layer.alert("请输入拍品名称");
			return;
		}
		if(!cou){
			layer.alert("请输入拍品数量");
			return;
		}
		if(!start){
			layer.alert("请选择起拍时间范围");
			return;
		}
		if(!imges){
			layer.alert("请上传拍品图片");
			return;
		}
		var param = {};
		param.sname = sname;
		param.stype = stype;
		param.descript = descript;
		param.imges = imges;
		param.price = price;
		param.cou = cou;
		param.start = start.substring(0,19);
		param.end = start.substring(21);
		console.log(param)
		Base.requestServer("saveGoods",param,successFun,true);
	});
}

function successFun(data) {
	if(data[Base.result_code]=Base.result_suc){
		layer.msg("保存成功");
		setTimeout(function () {
			var WIDout_trade_no = data.sid;
			var WIDsubject = sname;//名称
			var WIDtotal_amount = price*0.2;//金额
			var WIDbody = '1';//数量
			var param ={};
			param.WIDout_trade_no = WIDout_trade_no;
			param.WIDtotal_amount = WIDtotal_amount;
			param.WIDsubject = WIDsubject;
			param.WIDbody = WIDbody;
			Base.gotoView("/paymentConfirmation",param)
		},1000)
	}else{
		layer.msg("保存失败");
	}
}