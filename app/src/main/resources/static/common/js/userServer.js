var Base = {
    "serverPath": "http://localhost:8090",

};

Base.result_code = "code";
Base.result_msg = "message";
Base.result_suc = "1";
Base.result_fail = "-1";
/**
 * 接口对象，地址列表
 */Base.UrlPath = {
    "addStype": "/getData/addStype.do",//商品类别的添加
    "saveGoods": "/getData/saveGoods.do",//商品类别的添加
    "queryShopping":"/getData/queryShopping",
    "delType":"/getData/delType",
    "toExamine":"/getData/toExamine",//审核执行方法
    "upUState":"/getData/upUState",
    "queryMenu":"/getData/queryMenu",
    "topay":"/topay",
    "refund":"/refund",
};

Base.click = function (selector, func) {
    var eventName = "click";
    $(document).on(eventName, selector, function (e) {
        var $this = $(this);
        func($this, e);
    });
}


Base.gotoView = function (href, params) {
    var _p = "";
    if (params) {
        var i = 0;
        for (var o in params) {
            if (href.indexOf("?") != -1) { //有？
                _p += "&" + o + "=" + params[o];
            } else {
                if (i == 0) {
                    _p += "?" + o + "=" + params[o];
                } else {
                    _p += "&" + o + "=" + params[o];
                }
            }

            i++;
        }
        _p = encodeURI(encodeURI(_p));
    }
    window.location.href = href + _p;
};


Base.getParameter = function (key) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] == key) {
            return decodeURI(decodeURI(pair[1]));
        }
    }
    return (false);
};

/**json转str*/
Base.jsonToStr = function (json) {
    if (typeof json === 'object') {
        return JSON && JSON.stringify(json);
    }
};
/**
 * 字符串转json
 */
Base.strToJson = function (str) {
    if (typeof str === 'string') {
        return JSON && JSON.parse(str);
    }
};

/**
 * 请求服务
 * @method requestServer
 * @param {String} urlType 根据类型取得服务器地址 必选
 * @param {String} data 请求参数对象 可选，
 * @param {String} callback 回调方法  可选
 * @param {Boolean} mask 是否显示蒙层 可选 默认false
 * @param {String} type 请求方式:post,get 可选  默认get
 */
Base.requestServer = function (urlType, data, callback, mask, type) {
    urlType = $.trim(urlType);
    var url = "";
    if (urlType.indexOf("http") != -1) {
        url = urlType;
    } else {
        url = Base.serverPath + Base.UrlPath[urlType];
    }
    var data = data || {};
    var mask = mask || false;
    var type = type || "post";
    var ajax = $.ajax(url, {
        type: type,
        data: data,
        scriptCharset: 'utf-8',
        timeout: 40000,
        dataType: "json",
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        beforeSend: function (XMLHttpRequest) {
            if (mask) {

            }
        },
        complete: function (XMLHttpRequest, textStatus) {// 无论失败还是成功,都会调用这个方法

        },
        success: function (result) {
            if (mask) {
            }
//        	console.log("-----"+urlType+"-----"+JSON.stringify(result));
            if (typeof result == "string") {
                result = JSON.parse(result);
            }
            if (typeof callback == "function") {
                callback(result);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            if (mask) {
            }
            console.log(url);
            console.dir(data);
        }
    });
    return ajax;
};


Base.getRequest = function (name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return decodeURI(r[2]);
    return null;
}
/**
 * 判断值为空
 *
 * @method isEmpty
 * @param {string}
 *            value 待判断的值
 * @return {boolean}
 *            boolean 为空返回true;不为空返回false
 */
Base.isEmpty = function (value) {
    if (value == null || value == "" || value == undefined) {
        return true;
    }
    return false;
}

/*解析URL地址获取地址参数
 * name:代表参数名称key
 * accessurl:代表所要解析的地址
 */


Base.getUrlParam = function (name, accessurl) {
    //构造一个含有目标参数的正则表达式对象
    var prmarr = accessurl.split("?");
    if (prmarr.length > 1) {
        //匹配目标参数
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        var r = prmarr[1].match(reg);
        if (r != null) {
            return decodeURI(r[2]);
        }

    }
    return null;
}


/**
 * 页面布局
 * menueObject 数数组数据
 * 页面分几列
 * */
Base.getEle = function (menueObject, col) {
    var datastring = "";
    col = parseInt(col);
    var len = parseInt(menueObject.length % col);  //取余数
    var len1 = parseInt(menueObject.length / col);  //商
    if (len == 0) {
        for (var i = 0; i < menueObject.length;) {
            datastring += getString(menueObject, i, col);  //当正好有是3的倍数的时候(因为一个横排三个订单)
            i = i + col;
        }
    } else {   //当不是三的倍数的时候(因为一个横排三个订单)
        for (var i = 0; i < menueObject.length;) {
            if (i < (len1 * col)) {
                datastring += getString(menueObject, i, col);  //商部分都加3
                i = i + col;
            } else {
                datastring += getString(menueObject, i, len);  //余下多少，加多少
                i = i + len;
            }
        }
    }
    return datastring;
}


/*图标等级排序
 * leve  需要排序的等级
 * data ：需要排序的数据
 * ：isSort：是否需要排序
 * */
Base.getLevel = function (leve, data, isSort) {

    var leveObj = [];
    for (var i = 0; i < data.length; i++) {
        var menulev = data[i].menulevel; //菜单级别
        if (menulev == leve) {
            leveObj.push(data[i]);
        }
    }
    if (isSort == "T") {
        //冒泡排序
        for (var j = 0; j < leveObj.length - 1; j++) {
            for (var k = 0; k < leveObj.length - j - 1; k++) {
                var sortnok = parseInt(leveObj[k].sortno);
                var sortnok1 = parseInt(leveObj[k + 1].sortno);
                if (sortnok > sortnok1) {
                    var tmp = leveObj[k];
                    leveObj[k] = leveObj[k + 1];
                    leveObj[k + 1] = tmp;
                }

            }
        }
    }
    return leveObj;

}
Base.uploadFile = function (urlType, data, callback) {
    alert(data);
    var url = Base.serverPath + Base.UrlPath['fileUpLoad']
    var mask = true;
    var type = "post";
    var ajax = $.ajax(url, {
        type: type,
        data: data,
        scriptCharset: 'UTF-8',
        timeout: 40000,
        mimeType: "multipart/form-data",
        processData: false,
        contentType: false,
        beforeSend: function (XMLHttpRequest) {
            if (mask) {
            }
        },
        complete: function (XMLHttpRequest, textStatus) {// 无论失败还是成功,都会调用这个方法

        },
        success: function (result) {
            if (mask) {
            }
            console.log("----------" + result);
            if (typeof result == "string") {
                result = JSON.parse(result);
            }
            if (typeof callback == "function") {
                callback(result);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            if (mask) {
            }
        }
    });
    return ajax;
};

Base.getOrgCode = function (sicard) {
    var tag = sicard.slice(0, 1);
    var orgcode = '';
    switch (tag) {
        case 'A':
            orgcode = '520100';
            break;
        case 'B':
            orgcode = '520200';
            break;
        case 'C':
            orgcode = '520300';
            break;
        case 'D':
            orgcode = '522200';
            break;
        case 'E':
            orgcode = '522300';
            break;
        case 'F':
            orgcode = '520500';
            break;
        case 'G':
            orgcode = '520400';
            break;
        case 'H':
            orgcode = '522600';
            break;
        case 'J':
            orgcode = '522700';
            break;
        case 'K':
            orgcode = '529900';
            break;
        default:
    }
    return orgcode;
};
