var Base = {
     result_code:"code",
     result_msg:"message",
     result_suc:"1",
     result_fail:"-1",
     requestServer : function(url, param, callback, mask, async, error, type) {
         var param = param || {};
         var type = type || "post";
         if (mask) {
             $.blockUI({
                     message: '加载中...',
                     css: {
                         border: 'none',
                         width : '1.5rem',
                         margin:"auto auto",
                         padding: '15px',
                         backgroundColor: '#000',
                         '-webkit-border-radius': '10px',
                         '-moz-border-radius': '10px',
                         opacity: .5,
                         color: '#fff',
                         'border-radius' : '30px'
                     }
             });
         }
         if (async == undefined) {
             async = true;
         }
         var ajaxResult = {};
         var ajax = $.ajax({
             type: type,
             contentType: "application/x-www-form-urlencoded; charset=utf-8", // post提交乱码
             url: url,
             async: async,
             data: param,
             success: function (data) {
                 if (mask) {
                     $.unblockUI();
                 }
                 //alert("***" + data);
                 if (typeof data == "string") {
                     data = eval("(" + data + ")");
                 }
                 console.log("data:");
                 console.log(data);
                 // 返回的数据节点太深，在此做处理
                 var result = {};
                 var code = data[Base.result_code];
                 result[Base.result_code] = code;
                 result[Base.result_msg] = data[Base.result_msg];
                 if (code == Base.result_suc){
                     var count = data.data.result.recordcount;
                     result.count = count;
                     if (count !=0 ){
                         result.data = data.data.result.resultdata;
                     }
                 }
                 console.log("result:");
                 console.log(result);
                 ajaxResult = callback(result);
             },
             error: function (xhr, type) {
                 if (mask) {
                     $.unblockUI();
                 }
                 if (error) {
                     ajaxResult = error();
                     return;
                 }
             }
         });
     },
     setInputValue : function (data) {
         var inputValue = "";
         for(var key in data){
             inputValue = data[key];
             $("#"+key).val(inputValue);
         }
     }
}