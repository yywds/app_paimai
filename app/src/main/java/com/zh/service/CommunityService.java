package com.zh.service;

import com.zh.dao.CommunityDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Service
public class CommunityService {
    @Resource
    private CommunityDao communityDao;

    public int addStype(Map map){
        List<Map> list = communityDao.selStype(map);
        if(list.size()>0){
            return 0;
        }
        return communityDao.addStype(map);
    }

    public int delStype(Map map){
        return communityDao.delType(map);
    }

    public List<Map> selStype(Map map){
        return communityDao.selStype(map);
    }

    public int addShopping(Map map){
        return communityDao.addShopping(map);
    }

    public List queryShopping(HashMap<String, Object> pageParam) {
        return communityDao.queryShopping(pageParam);
    }

    public int conutType(){
        return communityDao.conutType();
    }

    public int countshopping(){
        return communityDao.countshopping();
    }

    public int toExamine(Map map){
        return communityDao.toExamine(map);
    }

    public List myShopping(Map map){return communityDao.myShopping(map);}

    public int countMyShopping(Map map){
        return communityDao.countMyShopping(map);
    }

    public int upOrder(Map map){
        return communityDao.upOrder(map);
    }

    public List queryMyOrder(Map map){
        return communityDao.queryMyOrder(map);
    }

    public Map refundParameter(Map map){
        return communityDao.refundParameter(map);
    }

    public int countmyMyOrder(HashMap<String, Object> param) {
        return communityDao.countmyMyOrder(param);
    }

    public List queryAllOrder(Map map){
        return communityDao.queryAllOrder(map);
    }

    public int countAllOrder(HashMap<String, Object> param) {
        return communityDao.countAllOrder(param);
    }

    public int refund(Map map){
        return communityDao.refund(map);
    }
}
