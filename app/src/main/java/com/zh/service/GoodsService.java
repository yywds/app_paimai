package com.zh.service;

import com.zh.comm.Const;
import com.zh.dao.GoodsDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
public class GoodsService {

    @Resource
    private GoodsDao dao;

    public List<Map<String, Object>> getGoodsDetail(Map map) {
        return dao.getGoodsDetail(map);
    }

    public List<Map<String, Object>> getAuctionInfo(Map map) {
        return dao.getAuctionInfo(map);
    }

    public Map saveAuction(Map param) {
        //检查是否已经过了竞拍时间
        Map resultMap = new HashMap();
        int rtn = dao.checkDate(param);
        if(rtn == 0){
            resultMap.put("success",false);
            resultMap.put("msg","竞拍时间已过！！！");
            return resultMap;
        }
        //检查上个竞拍人是否是本人
        rtn =dao.checkAuctionPerson(param);
        if(rtn == 1){
            resultMap.put("success",false);
            resultMap.put("msg","竞拍失败，原因：已经处于竞拍中或不能竞拍自己发布的商品");
            return resultMap;
        }
        //更新其他拍卖状态为 0
        dao.updateAuctionState(param);
        rtn = dao.saveAuction(param);
        if(rtn == 1){
            resultMap.put("success",true);
            resultMap.put("msg","竞拍成功！！！");
        }else{
            resultMap.put("success",false);
            resultMap.put("msg","程序出错，请重试！！！");
        }
        //更新竞拍
        return resultMap;
    }

    public Map searchGoods(Map param) {
        List<Map<String,Object>> list = dao.searchGoods(param);
        Map resultMap = new HashMap();
        if(list == null || list.isEmpty() ){
            resultMap.put("success",false);
        }else{
            resultMap.put("success",true);
            resultMap.put("list",list);
        }
        return resultMap;
    }

    public List<Map> myAucitonAdmin(HashMap<String, Object> param) {
        return dao.myAucitonAdmin(param);
    }

    public Integer countmyAuciton(HashMap<String,Object> param) {
        return dao.countmyAuciton(param);
    }

    public List<Map> getAppraise(HashMap<String, Object> param) {
        return dao.getAppraise(param);
    }

    public Integer saveAppraise(HashMap<String, Object> param) {
        return dao.saveAppraise(param);
    }

    public List<Map> getEndAuctionGoods() {
        return dao.getEndAuctionGoods();
    }

    public void creatOrder(List<Map> list) {
        //获取竞拍信息
        List<Map> auction = dao.getAuctionListBySid(list);
        for (Map m: auction) {
            String radom = Const.getPK(100);
            m.put("bid",radom);
            //根据竞拍信息，创建订单
            dao.creatOrder(m);
        }
    }

    public int updateGoodsState(List<Map> list) {

        return dao.updateGoodsState(list);
    }

    public int insertOrder(Map map) {
        return dao.creatOrder(map);
    }
}
