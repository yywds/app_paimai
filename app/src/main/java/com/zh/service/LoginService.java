package com.zh.service;

import com.zh.dao.LoginDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
public class LoginService {

    @Resource
    private LoginDao loginDao;

    //登录判断操作
    public List<Map> login(Map map){
        return loginDao.login(map);
    }

    public int register(Map param) {
        return loginDao.register(param);
    }

    public int checkUname(Map param) {
        return loginDao.checkUname(param);
    }

    public List userAdmin(Map map){return loginDao.userAdmin(map);}

    public int countUser(){
        return loginDao.countUser();
    }

    public int upUState(Map map){
        return loginDao.upUState(map);
    }

    public Map sumMyData(Map map){
        return loginDao.sumMyData(map);
    }
}

