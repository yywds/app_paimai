package com.zh.controller;

import com.alibaba.fastjson.JSONObject;
import com.zh.comm.Params;
import com.zh.service.CommunityService;
import com.zh.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.*;

@Controller
@RequestMapping("/goods")
public class goodsController extends BaseController{

    @Autowired
    private CommunityService communityService;
    @Autowired
    private GoodsService service;

    @RequestMapping("/getGoodsDetail.do")
    @ResponseBody
    public Map getGoodsDetail(ServletRequest request){
        Map map = Params.getParam(request);
        List<Map<String,Object>> list = service.getGoodsDetail(map);

        return list.get(0);
    }

    @RequestMapping("openDetail.do")
    public ModelAndView openDetail(ServletRequest request, ServletResponse response){
        Map map = Params.getParam(request);
        //商品信息
        List<Map<String,Object>> list = service.getGoodsDetail(map);
        //处理图片
        String image = (String) (list.get(0)).get("imges");
        List<Map<String,Object>> imageList = new ArrayList();

        if("".equals(image) || image == null){
            Map imageMap = new HashMap();
            imageMap.put("image","/image/noImage.jpg");
            imageList.add(imageMap);
        }else{
            //只有一张图片
            if(-1 == image.indexOf(",")){
                Map imageMap = new HashMap();
                imageMap.put("image","/picture/"+image);
                imageList.add(imageMap);
            }else{
                for (String str : image.split(",")) {
                    Map imageMap = new HashMap();
                    imageMap.put("image","/picture/"+str);
                    imageList.add(imageMap);
                } ;
            }

        }
        ModelAndView model = new ModelAndView("/view/goodsDetail");
        model.addObject("data",list.get(0));
        model.addObject("image",imageList);
        return model;
    }

    @RequestMapping("getAuctionInfo.do")
    @ResponseBody
    public Map getAuctionInfo(HttpServletRequest request){
        Map param = getPageParam(request);
        //获取拍卖信息
        List<Map<String, Object>> auction = service.getAuctionInfo(param);
        Map resultMap = new HashMap();
        if( auction == null || auction.isEmpty()){
            resultMap.put("success",false);
        }else{
            resultMap.put("success",true);
            resultMap.putAll(auction.get(0));
        }
        return resultMap;
    }

    @RequestMapping("openAuctionPage.do")
    public ModelAndView openAuctionPage(){
        return new ModelAndView("/view/auctionPage");
    }

    @RequestMapping("/saveAuction.do")
    @ResponseBody
    public Map saveAuction(ServletRequest request, HttpSession session){
        Map param = Params.getParam(request);
        String uid = (String) ((Map)session.getAttribute("user")).get("uid");
        param.put("uid",uid);
        return service.saveAuction(param);
    }

    @RequestMapping("/searchGoods.do")
    @ResponseBody
    public Map searchGoods(ServletRequest request, HttpSession session){
        Map param = Params.getParam(request);
        return service.searchGoods(param);
    }

    @RequestMapping("openMyAuction.do")
    public ModelAndView openMyAuction(ServletRequest request, HttpSession session){
        String uid = (String) ((Map)session.getAttribute("user")).get("uid");
        ModelAndView mode = new ModelAndView("/view/myAucionPage");
        return mode;
    }

    /**
     * 我的竞拍管理--分页获取数据
     * @param request
     * @param curr
     * @param nums
     * @return
     */
    @RequestMapping(value = "myAuctionAdmin.do",method = RequestMethod.GET)
    @ResponseBody
    public JSONObject myAuctionAdmin(HttpServletRequest request, int curr, int nums,HttpSession session) {
        JSONObject result = new JSONObject();
        HashMap<String,Object> param = getPageParam(request);
        if(curr==1){
            curr =0;
            nums -=1;
        }else {
            curr =nums*(curr-1);
            nums = nums*curr-1;
        }
        param.put("page",curr);
        param.put("limit",nums);
        String uid = (String) ((Map)session.getAttribute("user")).get("uid");
        param.put("uid",uid);
        List<Map> tList = service.myAucitonAdmin(param);
        result.put("code",0);
        result.put("msg","");
        result.put("count",service.countmyAuciton(param));
        result.put("data",tList);
        return result;
    }

    @RequestMapping("openGoodsDetailView.do")
    public ModelAndView openGoodsDetailView(ServletRequest request, ServletResponse response){
        Map map = Params.getParam(request);
        //商品信息
        List<Map<String,Object>> list = service.getGoodsDetail(map);
        //处理图片
        String image = (String) (list.get(0)).get("imges");
        List<Map<String,Object>> imageList = new ArrayList();

        if("".equals(image) || image == null){
            Map imageMap = new HashMap();
            imageMap.put("image","/image/noImage.jpg");
            imageList.add(imageMap);
        }else{
            //只有一张图片
            if(-1 == image.indexOf(",")){
                Map imageMap = new HashMap();
                imageMap.put("image","/picture/"+image);
                imageList.add(imageMap);
            }else{
                for (String str : image.split(",")) {
                    Map imageMap = new HashMap();
                    imageMap.put("image","/picture/"+str);
                    imageList.add(imageMap);
                } ;
            }

        }
        ModelAndView model = new ModelAndView("view/goodsDetailView");
        model.addObject("data",list.get(0));
        model.addObject("image",imageList);
        return model;
    }

    @RequestMapping(value = "getStypeList.do")
    @ResponseBody
    public JSONObject getStypeList(HttpServletRequest request) {
        JSONObject result = new JSONObject();
        HashMap<String,Object> param = new HashMap<String,Object>();
        param.put("state","1");
        List<Map> tList = communityService.selStype(param);
        result.put("success",true);
        result.put("list",tList);
        return result;
    }

    @RequestMapping(value = "getAppraise.do")
    @ResponseBody
    public JSONObject getAppraise(HttpServletRequest request) {
        JSONObject result = new JSONObject();
        HashMap<String,Object> param = getPageParam(request);
        List<Map> tList = service.getAppraise(param);
        result.put("success",true);
        result.put("list",tList);
        return result;
    }

    @RequestMapping(value = "saveAppraise.do")
    @ResponseBody
    public JSONObject saveAppraise(HttpServletRequest request,HttpSession session) {
        JSONObject result = new JSONObject();
        HashMap<String,Object> param = getPageParam(request);
        String uid = (String) ((Map)session.getAttribute("user")).get("uid");
        param.put("uid",uid);
        Integer rtn = service.saveAppraise(param);
        if(rtn == 1){
            result.put("success",true);
        }else {
            result.put("success",true);
            result.put("msg","保存评论失败，请重试！！！");
        }
        return result;
    }
}
