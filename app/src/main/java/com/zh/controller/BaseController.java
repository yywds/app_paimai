package com.zh.controller;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * 
* @ClassName: BaseController  
* @Description: TODO(这里用一句话描述这个类的作用)  
* @author olhdaodear  
*
 */
public class BaseController {
	
	
	/**
	  * @method getPageParam 方法 
	  * @describe <p>方法说明:获取参数对象 默认封装request参数 以及当前登陆用户信息</p>
	  * @return PageParam 
	 */
	public HashMap<String, Object> getPageParam(HttpServletRequest request) {
//		HttpServletRequest request = getRequest();
		try {
			request.setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		Map properties = request.getParameterMap();
		HashMap<String, Object> returnMap = new HashMap<>();
		Iterator entries = properties.entrySet().iterator();
		Map.Entry entry;
		String name = "";
		String value = "";
		StringBuilder sb = null;
		while (entries.hasNext()) {
			entry = (Map.Entry) entries.next();
			name = (String) entry.getKey();
			Object valueObj = entry.getValue();
			if(null == valueObj){ 
				value = ""; 
			}else if(valueObj instanceof String[]){
				sb = new StringBuilder();
				String[] values = (String[])valueObj;
				for(int i=0;i<values.length;i++){
					sb.append(values[i] + ",");
				}
				value = sb.substring(0, sb.length()-1); 
			}else{
				value = valueObj.toString(); 
			}
			returnMap.put(name, value.trim());
		}
		return returnMap;
	}

	/**
	  * @method getRequest 方法 
	  * @describe <p>方法说明:获取request对象</p>
	  * @return HttpServletRequest 
	 */
	public HttpServletRequest getRequest() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		return request;
	}
	
	/**
	  * @method getResponse 方法 
	  * @describe <p>方法说明:获取response对象</p>
	  * @return HttpServletResponse
	 */
	public HttpServletResponse getResponse() {
		HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getResponse();
		return response;
	}
	
}
 