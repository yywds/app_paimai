package com.zh.controller;

import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.AlipayResponse;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradeFastpayRefundQueryRequest;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.alipay.api.request.AlipayTradeRefundRequest;
import com.zh.comm.AlipayConfig;
import com.zh.comm.Const;
import com.zh.comm.ImgResult;
import com.zh.service.CommunityService;
import com.zh.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Controller
public class PayController extends BaseController {
    @Autowired
    private CommunityService communityService;
    @Autowired
    private GoodsService goodsService;

    /**
     * 支付确认
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "paymentConfirmation")
    public String paymentConfirmation(HttpServletRequest request, Model model) {
        Map map = getPageParam(request);
        BigDecimal money = new BigDecimal(map.get("WIDtotal_amount").toString());
        DecimalFormat df2 =new DecimalFormat("#.00");
        String WIDtotal_amount =df2.format(money);
        AlipayConfig.setPayType(map.get("WIDbody").toString());
        map.put("WIDtotal_amount",WIDtotal_amount);
        model.addAllAttributes(map);
        return "view/pay/paymentConfirmation";
    }

    /**
     * 支付确认
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "topay")
    public void topay(HttpServletRequest request, HttpServletResponse response) throws Exception {
        HashMap<String, Object> param = getPageParam(request);
        AlipayClient alipayClient = new DefaultAlipayClient(AlipayConfig.gatewayUrl, AlipayConfig.app_id, AlipayConfig.merchant_private_key, "json", AlipayConfig.charset, AlipayConfig.alipay_public_key, AlipayConfig.sign_type);

        //设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(AlipayConfig.return_url);
        alipayRequest.setNotifyUrl(AlipayConfig.notify_url);

        //商户订单号，商户网站订单系统中唯一订单号，必填
        String out_trade_no = (String) param.get("WIDout_trade_no");
        //付款金额，必填
        String total_amount = (String) param.get("WIDtotal_amount");
        //订单名称，必填
        String str = (String) param.get("WIDsubject");
        String subject = URLDecoder.decode(str, "utf-8");
        //商品描述，可空
        String body = "";

        alipayRequest.setBizContent("{\"out_trade_no\":\"" + out_trade_no + "\","
                + "\"total_amount\":\"" + total_amount + "\","
                + "\"subject\":\"" + subject + "\","
                + "\"body\":\"" + body + "\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");

        //若想给BizContent增加其他可选请求参数，以增加自定义超时时间参数timeout_express来举例说明
        //alipayRequest.setBizContent("{\"out_trade_no\":\""+ out_trade_no +"\","
        //		+ "\"total_amount\":\""+ total_amount +"\","
        //		+ "\"subject\":\""+ subject +"\","
        //		+ "\"body\":\""+ body +"\","
        //		+ "\"timeout_express\":\"10m\","
        //		+ "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");
        //请求参数可查阅【电脑网站支付的API文档-alipay.trade.page.pay-请求参数】章节
        StringBuilder sb = new StringBuilder();
        sb.append("<!DOCTYPE html>");
        sb.append("<html lang=\"en\">");
        sb.append("<head>").append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">").append("<title>支付</title>").append("</head>");
        sb.append("<body>");
        //请求
        String result = alipayClient.pageExecute(alipayRequest).getBody();
        sb.append(result);
        sb.append("</body>");
        sb.append("</html>");
        PrintWriter out = response.getWriter();
        //输出
        out.println(sb);
    }

    /**
     * @param request
     * @return
     * @throws AlipayApiException
     */
    @RequestMapping(value = "notify_url")
    public void notify_url(HttpServletRequest request, HttpServletResponse response) throws AlipayApiException, IOException {
        //获取支付宝POST过来反馈信息
        PrintWriter out = response.getWriter();
        Map<String, String> params = new HashMap<String, String>();
        Map<String, String[]> requestParams = request.getParameterMap();
        for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext(); ) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i]
                        : valueStr + values[i] + ",";
            }
            //乱码解决，这段代码在出现乱码时使用
            valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
            params.put(name, valueStr);
        }

        boolean signVerified = AlipaySignature.rsaCheckV1(params, AlipayConfig.alipay_public_key, AlipayConfig.charset, AlipayConfig.sign_type); //调用SDK验证签名

        //——请在这里编写您的程序（以下代码仅作参考）——

	/* 实际验证过程建议商户务必添加以下校验：
	1、需要验证该通知数据中的out_trade_no是否为商户系统中创建的订单号，
	2、判断total_amount是否确实为该订单的实际金额（即商户订单创建时的金额），
	3、校验通知中的seller_id（或者seller_email) 是否为out_trade_no这笔单据的对应的操作方（有的时候，一个商户可能有多个seller_id/seller_email）
	4、验证app_id是否为该商户本身。
	*/
        if (signVerified) {//验证成功
            //商户订单号
            String out_trade_no = new String(request.getParameter("out_trade_no").getBytes("ISO-8859-1"), "UTF-8");

            //支付宝交易号
            String trade_no = new String(request.getParameter("trade_no").getBytes("ISO-8859-1"), "UTF-8");

            //交易状态
            String trade_status = new String(request.getParameter("trade_status").getBytes("ISO-8859-1"), "UTF-8");

            if (trade_status.equals("TRADE_FINISHED")) {
                //判断该笔订单是否在商户网站中已经做过处理
                //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
                //如果有做过处理，不执行商户的业务程序

                //注意：
                //退款日期超过可退款期限后（如三个月可退款），支付宝系统发送该交易状态通知
            } else if (trade_status.equals("TRADE_SUCCESS")) {
                //判断该笔订单是否在商户网站中已经做过处理
                //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
                //如果有做过处理，不执行商户的业务程序

                //注意：
                //付款完成后，支付宝系统发送该交易状态通知
            }

            out.println("success");

        } else {//验证失败
            out.println("fail");

            //调试用，写文本函数记录程序运行情况是否正常
            //String sWord = AlipaySignature.getSignCheckContentV1(params);
            //AlipayConfig.logResult(sWord);
        }
    }

    /**
     * @param request
     * @return
     * @throws AlipayApiException
     */
    @RequestMapping(value = "return_url")
    public String return_url(HttpServletRequest request, HttpServletResponse response, Model model) throws AlipayApiException, IOException {
//        request.setCharacterEncoding("UTF-8");
//        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        //获取支付宝GET过来反馈信息
        Map<String, String> params = new HashMap<String, String>();
        Map<String, String[]> requestParams = request.getParameterMap();
        for (Iterator<String> iter = requestParams.keySet().iterator(); iter.hasNext(); ) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i]
                        : valueStr + values[i] + ",";
            }
            //乱码解决，这段代码在出现乱码时使用
            valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
            params.put(name, valueStr);
        }
        try {
            boolean signVerified = AlipaySignature.rsaCheckV1(params, AlipayConfig.alipay_public_key, AlipayConfig.charset, AlipayConfig.sign_type); //调用SDK验证签名

            //——请在这里编写您的程序（以下代码仅作参考）——
            if (signVerified) {
                //商户订单号
                String out_trade_no = new String(request.getParameter("out_trade_no").getBytes("ISO-8859-1"), "UTF-8");

                //支付宝交易号
                String trade_no = new String(request.getParameter("trade_no").getBytes("ISO-8859-1"), "UTF-8");

                //付款金额
                String total_amount = new String(request.getParameter("total_amount").getBytes("ISO-8859-1"), "UTF-8");
                String type = AlipayConfig.getPayType();
                Map map = new HashMap();
                map.put("bid", out_trade_no);
                map.put("out_trade_no", out_trade_no);
                map.put("trade_no", trade_no);
                map.put("paydate", Const.getDate());
                if (type.equals("2")) {
                    communityService.upOrder(map);
                } else {
                    HttpSession session = request.getSession(true);
                    Map user = null;
                    if(session != null){
                        user = (Map) session.getAttribute("user");
                    }
                    if(user != null){
                        map.put("uid", user.get("uid"));
                    }else{
                        map.put("uid", AlipayConfig.getUid());
                    }
                    String radom = Const.getPK(100);
                    map.put("bid", radom);
                    map.put("sid", out_trade_no);
                    map.put("type", type);
                    map.put("state", "2");
                    map.put("total_amount", total_amount);
                    goodsService.insertOrder(map);
                }
                model.addAttribute("code", "1");
                return "view/pay/notice";
            } else {
                model.addAttribute("code", "0");
                return "view/pay/notice";
            }
        } catch (Exception e) {
            System.err.println(e);
            model.addAttribute("code", "2");
            model.addAttribute("msg", "支付成功数据插入失败");
            return "view/pay/notice";
        }
    }

    /**
     * 退款接口
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "refund")
    public JSONObject refund(HttpServletRequest request) throws AlipayApiException {
        JSONObject jsonObject = new JSONObject();
        try {
            Map param = getPageParam(request);
            param = communityService.refundParameter(param);
            //获得初始化的AlipayClient
            AlipayClient alipayClient = new DefaultAlipayClient(AlipayConfig.gatewayUrl, AlipayConfig.app_id, AlipayConfig.merchant_private_key, "json", AlipayConfig.charset, AlipayConfig.alipay_public_key, AlipayConfig.sign_type);

            //设置请求参数
            AlipayTradeRefundRequest alipayRequest = new AlipayTradeRefundRequest();

            //商户订单号，商户网站订单系统中唯一订单号
            String out_trade_no = (String) param.get("out_trade_no");
            //支付宝交易号
            String trade_no = (String) param.get("trade_no");
            //请二选一设置
            //需要退款的金额，该金额不能大于订单金额，必填
            String refund_amount = (String) param.get("total_amount");
            //退款的原因说明
            String refund_reason = "保证金退款";
            //标识一次退款请求，同一笔交易多次退款需要保证唯一，如需部分退款，则此参数必传
            String out_request_no = "";

            alipayRequest.setBizContent("{\"out_trade_no\":\"" + out_trade_no + "\","
                    + "\"trade_no\":\"" + trade_no + "\","
                    + "\"refund_amount\":\"" + refund_amount + "\","
                    + "\"refund_reason\":\"" + refund_reason + "\","
                    + "\"out_request_no\":\"" + out_request_no + "\"}");

            //请求
            String result = alipayClient.execute(alipayRequest).getBody();
            System.out.println(result);
            JSONObject json = JSONObject.parseObject(result).getJSONObject("alipay_trade_refund_response");
            if (json.getString("code").equals(Const.ali_suc_code)) {
                Map map = new HashMap();
                map.put("refunddate", json.getString("gmt_refund_pay"));
                map.put("buyer_logon_id", json.getString("buyer_logon_id"));
                map.put("bid", param.get("bid"));
                int i = communityService.refund(map);
                jsonObject.put("code", 1);
            } else {
                jsonObject.put("code", 0);
            }
        } catch (Exception e) {
            System.err.println(e);
            jsonObject.put("code", 0);
        }
        return jsonObject;
    }
}
