package com.zh.controller;

import com.zh.comm.Params;
import com.zh.service.CommunityService;
import com.zh.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("page")
public class PageController {
    @Autowired
    private CommunityService communityService;
    @Autowired
    private LoginService loginService;

    @RequestMapping("logo.do")
    public String logo(){
        return "/view/logo";
    }

    @RequestMapping("addStype.do")
    public String addStype(){
        return "/view/addStype";
    }

    /**
     * 拍品上传页面
     * @return
     */
    @RequestMapping("addCommodity.do")
    public ModelAndView main(){
        ModelAndView mv = new ModelAndView();
        List<Map> tList = communityService.selStype(new HashMap());
        mv.addObject("sType",tList);
        mv.setViewName("/view/addCommodity");
        return mv;
    }

    @RequestMapping("openDetail")
    public String openDetail(ServletRequest request){
        Map param = Params.getParam(request);
        String str = "";
        return "view/goodsDetail";
    }

    /**
     * 类别列表
     * @return
     */
    @RequestMapping("stypeList.do")
    public String stypeList(){
        return "view/stypeList";
    }

    @RequestMapping("openDetail.do")
    public String openDetail(){
        return "/view/goodsDetail";
    }

    /**
     * 审核列表
     * @return
     */
    @RequestMapping(value = "toExamineList.do")
    public String toExamineList(){
        return "view/toExamineList";
    }

    /**
     * 个人资料
     * @return
     */
    @RequestMapping(value = "personalData.do")
    public String personalData(Model model, HttpServletRequest request){
        HttpSession session = request.getSession(true);
        Map map = (Map) session.getAttribute("user");
        model.addAllAttributes(loginService.sumMyData(map));
        return "view/personalData";
    }

    /**
     * 我的拍品
     * @return
     */
    @RequestMapping("myShopping.do")
    public String myShopping(){
        return "/view/myShopping";
    }

    /**
     * 我的订单
     * @return
     */
    @RequestMapping("myOrder.do")
    public String myOrder(){
        return "/view/pay/myOrder";
    }

    /**
     * 所有订单
     * @return
     */
    @RequestMapping("allOrder.do")
    public String allOrder(){
        return "/view/pay/allOrder";
    }
}
