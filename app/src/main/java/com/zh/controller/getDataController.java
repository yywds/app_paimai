package com.zh.controller;

import com.alibaba.fastjson.JSONObject;
import com.zh.comm.*;
import com.zh.service.CommunityService;
import com.zh.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping(value = "getData")
public class getDataController extends BaseController {

    @Autowired
    private CommunityService communityService;
    @Autowired
    private LoginService loginService;

    @RequestMapping(value = "addStype.do")
    public Integer addStype(HttpServletRequest request, HttpServletResponse response) {
        HashMap<String, Object> params = getPageParam(request);
        params.put("tid", Const.getPK(10));
        return communityService.addStype(params);
    }

    /**
     * 图片上传
     *
     * @param file
     * @param request
     * @return
     */
    @SuppressWarnings("deprecation")
    @RequestMapping(value = "upload")
    public ImgResult uplpad(MultipartFile file, HttpServletRequest request) {
        String desFilePath = "";
        String oriName = "";
        ImgResult result = new ImgResult();
        Map<String, String> dataMap = new HashMap<>();
        ImgResult imgResult = new ImgResult();
        try {
            // 1.获取原文件名
            oriName = file.getOriginalFilename();
            // 2.获取原文件图片后缀，以最后的.作为截取(.jpg)
            String extName = oriName.substring(oriName.lastIndexOf("."));
            // 3.生成自定义的新文件名，这里以UUID.jpg|png|xxx作为格式（可选操作，也可以不自定义新文件名）
            String uuid = UUID.randomUUID().toString();
            String newName = uuid + extName;
            // 4.获取要保存的路径文件夹
            String realPath = "C:/Java/app/app_paimai/image";
            File folder = new File(realPath);
            if (!folder.exists()) {
                folder.mkdirs();
            }
            // 5.保存
            desFilePath = realPath + "\\" + newName;
            File desFile = new File(desFilePath);
            file.transferTo(desFile);
            System.out.println(desFilePath);
            // 6.返回保存结果信息
            result.setCode(0);
            dataMap = new HashMap<>();
            dataMap.put("src", newName);
            result.setData(dataMap);
            result.setMsg(oriName + "上传成功！");
            return result;
        } catch (IllegalStateException e) {
            imgResult.setCode(1);
            System.out.println(desFilePath + "图片保存失败");
            return imgResult;
        } catch (IOException e) {
            imgResult.setCode(-1);
            System.out.println(desFilePath + "图片保存失败--IO异常");
            return imgResult;
        }
    }

    /**
     * 拍品添加
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "saveGoods.do", method = RequestMethod.POST)
    private JSONObject saveImgInfo(HttpServletRequest request, HttpSession session) {
        JSONObject jsonObject = new JSONObject();
        try{
            HashMap<String, Object> map = getPageParam(request);
            String uid = (String) ((Map) session.getAttribute("user")).get("uid");
            map.put("sid", Const.getPK(10));
            map.put("userid", uid);
            int code =  communityService.addShopping(map);
            jsonObject.put("code",code);
            jsonObject.put("sid",map.get("sid"));
            jsonObject.put("sname",map.get("sname"));
            return jsonObject;
        } catch (Exception e) {
            System.err.println(e);
            jsonObject.put("code",0);
            return jsonObject;
        }
    }

    /**
     * 拍品列表
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "queryShopping", method = RequestMethod.GET)
    private String queryShopping(HttpServletRequest request, int curr, int nums) {
        HashMap<String, Object> pageParam = getPageParam(request);
        JSONObject jsonObject = new JSONObject();
        try {
            if(curr==1){
                curr =0;
                nums -=1;
            }else {
                curr =nums*(curr-1);
                nums = nums*curr-1;
            }
            pageParam.put("page", curr);
            pageParam.put("limit", nums);
            List sList = communityService.queryShopping(pageParam);
            jsonObject.put("code",0);
            jsonObject.put("msg","");
            jsonObject.put("count",communityService.countshopping());
            jsonObject.put("data", sList);
        } catch (Exception e) {
            jsonObject.put("code", 1);
        }
        return jsonObject.toString();
    }

    /**
     * 拍品类别--分页获取数据
     * @param request
     * @param curr
     * @param nums
     * @return
     */
    @RequestMapping(value = "stypeList", method = RequestMethod.GET)
    public JSONObject stypeList(HttpServletRequest request, int curr, int nums) {
        JSONObject result = new JSONObject();
        HashMap<String,Object> param = new HashMap<String,Object>();
        if(curr==1){
            curr =0;
            nums -=1;
        }else {
            curr =nums*(curr-1);
            nums = nums*curr-1;
        }
        param.put("page",curr);
        param.put("limit",nums);
        List<Map> tList = communityService.selStype(param);
        result.put("code", 0);
        result.put("msg", "");
        result.put("count", communityService.conutType());
        result.put("data", tList);
        return result;
    }

    /**
     * 删除--类别
     *
     * @param request
     * @return
     */
    @RequestMapping("delType")
    public int delType(HttpServletRequest request){
        HashMap<String,Object> param = getPageParam(request);
        return communityService.delStype(param);
    }

    /**
     * 审核执行--拍品
     *
     * @param request
     * @return
     */
    @RequestMapping("toExamine")
    public int toExamine(HttpServletRequest request){
        HashMap<String,Object> param = getPageParam(request);
        return communityService.toExamine(param);
    }

    /**
     * 用戶管理--分页获取数据
     * @param request
     * @param curr
     * @param nums
     * @return
     */
    @RequestMapping(value = "userAdmin", method = RequestMethod.GET)
    public JSONObject userAdmin(HttpServletRequest request, int curr, int nums) {
        JSONObject result = new JSONObject();
        HashMap<String, Object> param = new HashMap<String, Object>();
        if (curr == 1) {
            curr = 0;
            nums -= 1;
        } else {
            curr = nums * (curr - 1);
            nums = nums * curr - 1;
        }
        param.put("page", curr);
        param.put("limit", nums);
        List<Map> tList = loginService.userAdmin(param);
        result.put("code", 0);
        result.put("msg", "");
        result.put("count", loginService.countUser());
        result.put("data", tList);
        return result;
    }

    @RequestMapping(value = "upUState")
    public int upUState(HttpServletRequest request) {
        HashMap<String, Object> param = getPageParam(request);
        HttpSession session = request.getSession(true);
        if(!param.containsKey("state")){
            session.removeAttribute("user");
        }
        return loginService.upUState(param);
    }

    @RequestMapping(value = "queryMenu")
    public List queryMenu(HttpServletRequest request) {
        HashMap<String, Object> param = getPageParam(request);
        return communityService.selStype(param);
    }

    @RequestMapping(value = "myShopping")
    public JSONObject myShopping(HttpServletRequest request, int curr, int nums) {
        JSONObject result = new JSONObject();
        HashMap<String, Object> param = getPageParam(request);
        HttpSession session = request.getSession(true);
        Map map = (Map) session.getAttribute("user");
        if (curr == 1) {
            curr = 0;
            nums -= 1;
        } else {
            curr = nums * (curr - 1);
            nums = nums * curr - 1;
        }
        param.put("uid", map.get("uid"));
        param.put("page", curr);
        param.put("limit", nums);
        List<Map> tList = communityService.myShopping(param);
        result.put("code", 0);
        result.put("msg", "");
        result.put("count", communityService.countMyShopping(param));
        result.put("data", tList);
        return result;
    }

    /**
     * 订单数据列表
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "toMyOrder", method = RequestMethod.GET)
    private JSONObject toMyOrder(HttpServletRequest request, int curr, int nums) {
        JSONObject result = new JSONObject();
        HashMap<String,Object> param = getPageParam(request);
        if(curr==1){
            curr =0;
            nums -=1;
        }else {
            curr =nums*(curr-1);
            nums = nums*curr-1;
        }
        param.put("page",curr);
        param.put("limit",nums);
        HttpSession session = request.getSession(true);
        Map user = (Map) session.getAttribute("user");
        param.put("uid",user.get("uid"));
        List<Map> tList = communityService.queryMyOrder(param);
        result.put("code",0);
        result.put("msg","");
        result.put("count",communityService.countmyMyOrder(param));
        result.put("data",tList);
        return result;
    }

    /**
     * 订单数据列表
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "toAllOrder", method = RequestMethod.GET)
    private JSONObject toAllOrder(HttpServletRequest request, int curr, int nums) {
        JSONObject result = new JSONObject();
        HashMap<String,Object> param = getPageParam(request);
        if(curr==1){
            curr =0;
            nums -=1;
        }else {
            curr =nums*(curr-1);
            nums = nums*curr-1;
        }
        param.put("page",curr);
        param.put("limit",nums);
        List<Map> tList = communityService.queryAllOrder(param);
        result.put("code",0);
        result.put("msg","");
        result.put("count",communityService.countAllOrder(param));
        result.put("data",tList);
        return result;
    }
}
