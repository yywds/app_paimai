package com.zh.dao;

import org.apache.ibatis.annotations.Mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Mapper
public interface GoodsDao {

    List<Map<String, Object>> getGoodsDetail(Map map);

    List<Map<String, Object>> getAuctionInfo(Map map);

    int checkDate(Map param);

    int saveAuction(Map param);

    void updateAuctionState(Map param);

    int checkAuctionPerson(Map param);

    List<Map<String, Object>> searchGoods(Map param);

    Integer countmyAuciton(HashMap<String,Object> param);

    List<Map> myAucitonAdmin(HashMap<String, Object> param);

    List<Map> getAppraise(HashMap<String, Object> param);

    Integer saveAppraise(HashMap<String, Object> param);

    List<Map> getEndAuctionGoods();

    int updateGoodsState(List<Map> list);

    int creatOrder(Map map);

    List<Map> getAuctionListBySid(List<Map> list);
}
