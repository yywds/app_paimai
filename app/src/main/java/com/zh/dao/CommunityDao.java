package com.zh.dao;

import org.apache.ibatis.annotations.Mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 商品类---dao
 */
@Mapper
public interface CommunityDao {
    //添加商品类别
    int addStype(Map map);
    //删除商品类别
    int delType(Map map);
    //商品类别列表
    List<Map> selStype(Map map);
    //添加商品
    int addShopping(Map map);

    List queryShopping(HashMap<String, Object> pageParam);

    int conutType();

    int countshopping();

    int toExamine(Map map);

    List myShopping(Map map);

    int countMyShopping(Map map);

    int upOrder(Map map);

    List queryMyOrder(Map map);

    Map refundParameter(Map map);

    int countmyMyOrder(HashMap<String, Object> param);

    List queryAllOrder(Map map);

    int countAllOrder(HashMap<String, Object> param);

    int refund(Map map);
}
