package com.zh.quartz;

import com.zh.service.GoodsService;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
@Configurable
@EnableScheduling
public class MyQuartz { //implements Job

    @Autowired
    private GoodsService service;

    public void start(){
        System.err.println("定时任务开始执行。"+new Date());
        //获取已结束竞拍的商品
        List<Map> list = service.getEndAuctionGoods();
        if(list != null && !list.isEmpty()){
            //根据竞拍信息生成订单信息
            service.creatOrder(list);
            //将商品的状态更改为已结束
             service.updateGoodsState(list);
        }
        System.err.println("定时任务结束执行。"+new Date());
    }
}
