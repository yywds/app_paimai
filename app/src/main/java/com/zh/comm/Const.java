package com.zh.comm;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.alipay.api.request.AlipayTradeRefundApplyRequest;
import com.alipay.api.request.AlipayTradeRefundRequest;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Const {
    public static String ali_suc_code = "10000";
    public static String getPK(int count){
        Date dt = new Date(); //当前日期
        Random id=new Random();
        int num=id.nextInt(count);
        num++;
        SimpleDateFormat df=new SimpleDateFormat("yyyyMMddHHmmss");
        String dtString=df.format(dt);
        DecimalFormat dcf=new DecimalFormat("'"+dtString+"'000");
        String value=dcf.format(num);
        return value;
    }

    public void getDataTable(int curr,int nums){
        if(curr==1){
            curr =0;
            nums -=1;
        }else {
            curr =nums*(curr-1);
            nums = nums*curr-1;
        }
    }

    public static String getDate(){
        Date dNow = new Date( );
        SimpleDateFormat ft =
                new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
        return ft.format(dNow);
    }

    public static void main(String[] args) {//20200326205839008
//        alipay_trade_refund_response
        System.out.println(getPK(10));
    }
}
