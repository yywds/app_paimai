package com.zh.comm;

import javax.servlet.ServletRequest;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Params {

    /**
     * 使用getParameterMap获取到的参数（原value为String[]），将value转为String
     * @param request
     * @return
     */
    public static Map getParam(ServletRequest request){
        Map map = request.getParameterMap();
        Map<String,String> resultMap = new HashMap();
        if(map != null && !map.isEmpty()){
            Iterator it  =map.entrySet().iterator();
            String val;
            while (it.hasNext()){
                Map.Entry entry = (Map.Entry)it.next();
                val = ((String[])entry.getValue())[0];
                resultMap.put((String) entry.getKey(),(String) val);
            }
        }
        return resultMap;
    }
}
