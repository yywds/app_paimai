package com.zh.comm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class MyTimeTask {

    //基于spring的 scheduling
    //启动类需要添加注解@EnableScheduling
    /**
     * 自动扫描，启动时间点之后5秒执行一次
     */
//    @Scheduled(fixedRate=30000)
//    public void getCurrentDate() {
//        System.out.print("每多少秒执行一次");
//    }
}
